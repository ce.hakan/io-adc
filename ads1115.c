/*
 *
 */

#include <stdio.h>
#include <sys/types.h> 
#include <sys/stat.h>  
#include <fcntl.h>    
#include <unistd.h>
#include <stdlib.h>
#include <inttypes.h>  
#include <linux/i2c-dev.h>
#include "ads1115.h"

int fd;
int ads_address = 0x48;
int16_t val;

uint8_t writeBuffer[3];
uint8_t readBuffer[2];

float myFloat;

const float VPS = 4.096 / 32768.0; //volts per step

/*
The resolution of the ADC in single ended 
mode we have 15 bit rather than 16 bit resolution, 
the 16th bit being the sign of the differential reading.
*/

float read_ADS1115(unsigned short pChannel) {
    if((fd = open("/dev/i2c-0", O_RDWR)) < 0) {
        printf("Error: Could not open device! %d\n", fd);
        return -200;
    }

    if(ioctl(fd, I2C_SLAVE, ads_address) < 0) {
        printf("Error: Could not find device on address!\n");
        return -200;
    }

    // set config register and start conversion
    // ANC1 and GND, 4.096V, 128s/s

    writeBuffer[0] = 1; // config register is 1

    if(pChannel == 0) writeBuffer[1] = 0b11000011;
    else if(pChannel == 1) writeBuffer[1] = 0b11010011;
    else if(pChannel == 2) writeBuffer[1] = 0b11100011;
    else if(pChannel == 3) writeBuffer[1] = 0b11110011;

    //writeBuffer[1] = 0b11000011; // bit 15-8 0xC3
    // bit 15 flag bit for single shot
    // Bits 14-12 input selection:
    // 100 ANC0; 101 ANC1; 110 ANC2; 111 ANC3
    // Bits 11-9 Amp gain. Default to 010 here 001 P19
    // Bit 8 Operational mode of the ADS1115.
    // 0 : Continuous conversion mode
    // 1 : Power-down single-shot mode (default)

    writeBuffer[2] = 0b10000101; // bits 7-0 0x85
    // Bits 7-5 data rate default to 100 for 128SPS
    // Bits 4-0  comparator functions see spec sheet.

    // begin conversion
    if(write(fd, writeBuffer, 3) != 3) {
        perror("Write to register 1");
        return -200;
    }

    // wait for conversion complete
    // checking bit 15

    do {
        if(read(fd, writeBuffer, 2) != 2) {
            perror("Read conversion");
            exit(-1);
        }
    }
    while((writeBuffer[0] & 0x80) == 0);

    // read conversion register
    // write register pointer first
    readBuffer[0] = 0; // conversion register is 0

    if(write(fd, readBuffer, 1) != 1) {
        perror("Write register select");
        return -200;
    }

    // read 2 bytes
    if(read(fd, readBuffer, 2) != 2) {
        perror("Read conversion");
        return -200;
    }

    // convert display results
    val = readBuffer[0] << 8 | readBuffer[1];

    if(val < 0) val = 0;

    myFloat = val*VPS; // convert to voltage

    myFloat = ((myFloat/3.74)*13.74);

    printf("Channel: %d Values: HEX 0x%02x DEC %d reading %4.3f volts.\n", pChannel, val, val, myFloat);

    close(fd);

    return myFloat;
}
