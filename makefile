CC = mipsel-openwrt-linux-gcc

CFLAGS = -g -Wall

OBJ = main.o MCP9808.o io.o ads1115.o

TARGET = io-adc

# top-level rule to compile the sholw program.
all: $(TARGET)

$(TARGET): $(OBJ)
	$(CC) $(CFLAGS) $(OBJ) -o $(TARGET)
	rm -f main.o ads1115.o MCP9808.o io.o

main.o: main.c io.h MCP9808.h ads1115.h
	$(CC) $(CFLAGS) -c main.c

io.o: io.c io.h
	$(CC) $(CFLAGS) -c io.c

MCP9808.o: MCP9808.c MCP9808.h
	$(CC) $(CFLAGS) -c MCP9808.c

ads1115.o: ads1115.c ads1115.h
	$(CC) $(CFLAGS) -c ads1115.c

# rule for cleaning files generated during compilations.
clean:
	/bin/rm -f io-adc main.o ads1115.o MCP9808.o io.o

install:
        scp io-adc root@192.168.100.1:/usr/bin/
