/*
 *
 */

#include	<sys/types.h>	/* basic system data types */
#include	<sys/socket.h>	/* basic socket definitions */
#include	<sys/time.h>	/* timeval{} for select() */
#include	<time.h>	/* timespec{} for pselect() */
#include	<netinet/in.h>	/* sockaddr_in{} and other Internet defns */
#include 	<net/if.h>  
#include	<arpa/inet.h>	/* inet(3) functions */
#include 	<sys/ioctl.h>
#include	<errno.h>
#include	<fcntl.h>	/* for nonblocking */
#include	<netdb.h>
#include	<signal.h>
#include 	<termios.h> 
#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>
#include	<sys/stat.h>	/* for S_xxx file mode constants */
#include	<sys/uio.h>	/* for iovec{} and readv/writev */
#include	<unistd.h>
#include	<sys/wait.h>
#include	<sys/un.h>	/* for Unix domain sockets */

#include 	"io.h"
#include 	"ads1115.h"
#include 	"MCP9808.h"

#define SERV_PORT   1111

unsigned char   pDataRx[250];
unsigned char   pDataTx[50];
float           channelADC[4];
float           MCP9808Temp;

static char read_GPIO(void) {
    char input;

    if((pDataRx[9] >= '0') && (pDataRx[9] <= 'A')) {
        if(pDataRx[9] == '0') read_INPUT(0, '0', &input);
        else if(pDataRx[9] == '1') read_INPUT(0, '1', &input);
        else if(pDataRx[9] == '2') read_INPUT(0, '2', &input);
        else if(pDataRx[9] == '3') read_INPUT(0, '3', &input);
        else if(pDataRx[9] == '4') read_INPUT(1, '4', &input);
        else if(pDataRx[9] == '5') read_INPUT(1, '5', &input);
        else if(pDataRx[9] == '6') read_INPUT(1, '6', &input);
        else if(pDataRx[9] == '7') read_INPUT(1, '7', &input);
        else if(pDataRx[9] == '8') read_INPUT(1, '8', &input);
        else if(pDataRx[9] == '9') read_INPUT(1, '9', &input);
        else if(pDataRx[9] == 'A') read_INPUT(1, 'A', &input);

        if(input) return '1';
        else return '0';
    }
    return -1;
}

static char set_GPIO(void) {
    char ret;

    if((pDataRx[10] == ',') && (pDataRx[9] >= '4') && (pDataRx[9] <= 'A')) {
        if((pDataRx[11] == '0') || (pDataRx[11] == '1')) {
            if(pDataRx[9] == '4') write_OUTPUT('4', pDataRx[11]);
            else if(pDataRx[9] == '5') write_OUTPUT('5', pDataRx[11]);
            else if(pDataRx[9] == '6') write_OUTPUT('6', pDataRx[11]);
            else if(pDataRx[9] == '7') write_OUTPUT('7', pDataRx[11]);
            else if(pDataRx[9] == '8') write_OUTPUT('8', pDataRx[11]);
            else if(pDataRx[9] == '9') write_OUTPUT('9', pDataRx[11]);
            else if(pDataRx[9] == 'A') write_OUTPUT('A', pDataRx[11]);
            return 1;
        }
        else return -1;
    }

    return -1;
}

static void sig_chld(int signo) {
    pid_t   pid;
    int     stat;

    while((pid = waitpid(-1, &stat, WNOHANG)) > 0) {
        printf("child %d terminated\n", pid);
    }
    return;
}

int main(int argc, char *argv[]) {
    int                 listenfd, connfd;
    pid_t               childpid;
    socklen_t           clilen;
    struct sockaddr_in  cliaddr, servaddr;
    void                sig_chld(int);
    unsigned short      n;
    int                 i, jk;
    char                flag;

    listenfd = socket(AF_INET, SOCK_STREAM, 0);

    if(setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &(int){1}, sizeof(int)) < 0) {
        perror("setsockopt(SO_REUSEADDR) failed");
    }

    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family         = AF_INET;
    servaddr.sin_addr.s_addr    = htonl(INADDR_ANY);
    servaddr.sin_port           = htons(SERV_PORT);

    bind(listenfd, (struct sockaddr *) &servaddr, sizeof(servaddr));

    listen(listenfd, 1024);

    signal(SIGCHLD, sig_chld); /* must call waitpid() */

    /*
    char sysBuff[60];
        sprintf(sysBuff, "echo 11 > /sys/class/gpio/export");
        system(sysBuff);
        for(jk=0; jk<sizeof(sysBuff); jk++) sysBuff[jk] = 0;
        sprintf(sysBuff, "echo out > /sys/class/gpio/gpio11/direction");
        system(sysBuff);
        for(jk=0; jk<sizeof(sysBuff); jk++) sysBuff[jk] = 0;
        sprintf(sysBuff, "echo 1 > /sys/class/gpio/gpio11/value");
        system(sysBuff);
        for(jk=0; jk<sizeof(sysBuff); jk++) sysBuff[jk] = 0;
        sleep(2);
        sprintf(sysBuff, "echo 0 > /sys/class/gpio/gpio11/value");
        system(sysBuff);
        for(jk=0; jk<sizeof(sysBuff); jk++) sysBuff[jk] = 0;


        write_OUTPUT('4','0');
        write_OUTPUT('5','0');
        write_OUTPUT('6','0');
        write_OUTPUT('7','0');
        write_OUTPUT('8','0');
        write_OUTPUT('9','0');
        write_OUTPUT('A','0');
        write_OUTPUT('B','0');
    */

    for(;;) {
        clilen = sizeof(cliaddr);
        if((connfd = accept(listenfd, (struct sockaddr *) &cliaddr, &clilen)) < 0) {
            if(errno == EINTR) continue; // back to for()
        }

        if((childpid = fork()) == 0) {
            // child process
            while(1) {
                for(i=0; i<sizeof(pDataRx); i++) pDataRx[i] = 0;
                for(i=0; i<sizeof(pDataTx); i++) pDataTx[i] = 0;

                n = recvfrom(connfd, pDataRx, sizeof(pDataRx), 0, (struct sockaddr *) &cliaddr, &clilen);

                if(n>0) {
                    if(strncmp(pDataRx, "AT+WGPIO=", 9) == 0) {
                        if(set_GPIO()>0) {
                            if(pDataRx[9] == 'A') sprintf(pDataTx, "+WGPIO=A,%d\r\n", pDataRx[11]-'0');
                            else sprintf(pDataTx, "+WGPIO=%d,%d\r\n", pDataRx[9]-'0', pDataRx[11]-'0');

                            write(connfd, pDataTx, 12);
                            flag = 1;
                        }
                        else flag = 0;
                    }
                    else if(strncmp(pDataRx, "AT+RGPIO=", 9) == 0) {
                        char ret;
                        ret = read_GPIO();

                        if((ret >= '0') && (ret <= '1')) {
                            if(pDataRx[9] == 'A') sprintf(pDataTx, "+RGPIO=A, %d\r\n", ret-'0';
                                    else sprintf(pDataTx, "+RGPIO=%d,%d\r\n", pDataRx[9]-'0', ret-'0');

                            write(connfd, pDataTx, 12);
                            flag = 1;
                        }
                        else flag = 0;
                    }
                    else if(strncmp(pDataRx, "AT+ADC=", 7) == 0) {
                        switch (pDataRx[7])
                        {
                        case '0':
                            channelADC[0] = read_ADS1115(0);
                            break;
                        case '1':
                            channelADC[1] = read_ADS1115(1);
                            break;
                        default:
                            pDataRx[7] = '4'; // Unexpected Entry
                            flag = 0;
                            break;
                        }

                        if((pDataRx[7] >= '0') && pDataRx[7] <= '1') {
                            sprintf(pDataTx, "+ADC=%d,%4.2fmV\r\n", (pDatarX[7]-'0'), (channelADC[pDataRx[7]-'0'])*1000);
                            write(connfd, pDataTx, 18);
                            flag = 1;
                        }
                        else {
                            sprintf(pDataTx, "+ADC=%d,%4.2fmV\r\n", (pDataRx[7]-'0'), (channelADC[pDataRx[7]-'0'])*1000);
                            write(connfd, pDataTx, 18);
                            flag = 0;
                        }
                    }
                    else if(strncmp(pDataRx, "AT+TEMP", 7) == 0) {
                        MCP9808Temp = read_MCP9808();
                        if(MCP9808Temp == -200) {
                            printf("TEMP: Sensor Error\n");
                            sprintf(pDataTx, "+TEMP=Sensor Error\r\n");
                            write(connfd, pDataTx, 21);
                            flag = 0;
                        }
                        else {
                            printf("TEMP:%4.2f\n", MCP9808Temp);
                            sprintf(pDataTx, "+TEMP=%4.2f\r\n", MCP9808Temp);
                            write(connfd, pDataTx, 16);
                            flag = 1;
                        }
                    }
                    else flag = 0;

                    if(flag) write(connfd, "OK\r\n", 4);
                    else write(connfd, "ERROR\r\n", 7);
                }
                else {
                    close(listenfd); // close listenning socket
                    close(connfd):   // parent closes connected socket

                        exit(0);
                }
            }
        }
    }
}
