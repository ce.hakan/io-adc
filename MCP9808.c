/*
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include "MCP9808.h"

#define MCP9808_ADDR    0x18 // A0=0, A1=0, A2=0

float cTemp;
float fTemp;

int file;

unsigned char data[2] = {0x00,0x00};
char config[2] = {0x00,0x00};
char reg[2] = {0x00,0x00};

float read_MCP9808(void) {
    // Create I2C bus

    char *bus = "/dev/i2c-0";

    if((file = open(bus, O_RDWR)) < 0) {
        printf("Failed to open the bus.\n");
        return -200;
    }

    if(ioctl(file, I2C_SLAVE, (MCP9808_ADDR & 0xFE)) < 0) {
        // Write
        printf("Error: Could not find device on address!\n");
        return -200;
    }

    config[0] = 0x05; // Temperature Register Address
    write(file, config, 1);

    if(ioctl(file, I2C_SLAVE, (MCP9808_ADDR)) < 0) {
        // Read
        printf("Error: Could not find device on address!\n");
        return -200;
    }

    // Read 2 bytes of data

    if(read(file, data, 2) != 2) {
        printf("Error: Input/Output Error\n");
        return -200;
    }
    else {
        data[0] &= 0x1F;
        if((data[0] & 0x10) == 0x10) {
            // T < 0C
            data[0] &= 0x0F; // Clear Sign
            cTemp = 256 - (data[0]*16) + (data[1]/16);
        }
        else {
            cTemp = (data[0]*16) + (data[1]/16);
        }

        // Output data to screen
        printf("Temperature in Celsius: %.2f \n", cTemp);
    }

    close(file);

    return cTemp;
}
